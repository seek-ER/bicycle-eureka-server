FROM openjdk:11-jdk-slim

# 配置参数
ARG JAR_FILE=target/*.jar
# 将编译构建得到的jar文件复制到镜像空间中
COPY ${JAR_FILE} application.jar

ENV ELASTIC_APM_VERSION "1.19.0"
RUN  apt-get update \
  && apt-get install -y wget \
  && rm -rf /var/lib/apt/lists/*
RUN wget -O /apm-agent.jar https://search.maven.org/remotecontent?filepath=co/elastic/apm/elastic-apm-agent/$ELASTIC_APM_VERSION/elastic-apm-agent-$ELASTIC_APM_VERSION.jar

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar","application.jar"]

EXPOSE 8000